﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModel;
using PrintChequeBL;

namespace PrintCheque.Forms
{
    
    public partial class SearchPayee : Window
    {
        public PS_CHEQUE_PAYEE SelectedPayeeName;
        public SearchPayee()
        {
            InitializeComponent();
            ShowAllPayee();
        }
        private void ShowAllPayee()
        {
            PayeeDataGrid.ItemsSource = null;
            var payee = BussinessLayerService.PayeeCheque().GetAllPayee();
            if (payee != null) PayeeDataGrid.ItemsSource = payee;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(SearchTextBox.Text)) return;
                PayeeDataGrid.ItemsSource = null;
                PayeeDataGrid.ItemsSource = BussinessLayerService.PayeeCheque().GetListPayeeByFilter(SearchTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(SearchTextBox.Text)) return;
                PayeeDataGrid.ItemsSource = null;
                PayeeDataGrid.ItemsSource = BussinessLayerService.PayeeCheque().GetListPayeeByFilter(SearchTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PayeeDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SelectedPayeeName = (PS_CHEQUE_PAYEE)PayeeDataGrid.SelectedItem;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
