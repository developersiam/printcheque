﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DomainModel;
using PrintChequeBL;

namespace PrintCheque.Forms
{
    /// <summary>
    /// Interaction logic for PrintCheque.xaml
    /// </summary>
    public partial class PrintCheque : Page
    {
        int ID_Payee;
        string Name_Payee = "";
        string user = "";

        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;

        public PrintCheque(string User)
        {
            InitializeComponent();
            user = User;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(issueDatePicker.Text)) throw new Exception("กรุณาระบุวันที่จ่ายเช็ค");
                if (string.IsNullOrEmpty(PayeeTextBox.Text)) throw new Exception("กรุณาระบุบริษัทที่ต้องการจ่ายเช็ค");
                if (string.IsNullOrEmpty(AmountTextBox.Text)) throw new Exception("กรุณาใส่จำนวนเงิน");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลใช่หรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newRecord = new PS_CHEQUE_DETAILS
                    {
                        Paid_date = (DateTime)issueDatePicker.SelectedDate,
                        Id = ID_Payee,
                        Amount = Convert.ToDecimal(AmountTextBox.Text),
                        Remark = RemarkTextBox.Text,
                        Ddate = DateTime.Now,
                        Ttime = DateTime.Now,
                        dtrecord = DateTime.Now,
                        dtuser = user
                    };
                    BussinessLayerService.PayeeChequeDetails().InsertChequeDetails(newRecord);   
                    //Print
                    printtoExcle();

                    MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Completed!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    //Clear
                    issueDatePicker.Text = "";
                    PayeeTextBox.Text = "";
                    AmountTextBox.Text = "";
                    RemarkTextBox.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void printtoExcle()
        {
            excelWorkBook = excelApplication.Workbooks.Open(@"C:\PrintCheque\Excel1.xls");
            excelWorkSheet = excelWorkBook.ActiveSheet;
            excelWorkSheet.PageSetup.Zoom = false;
            excelWorkSheet.PageSetup.FitToPagesWide = 1;
            excelApplication.Visible = false;

            var issueDate = issueDatePicker.SelectedDate.Value;
            var _date = issueDate.ToString("dd");
            var _month = issueDate.ToString("MM");
            var _year = issueDate.ToString("yyyy");

            PutDataintoCell("F1", _date[0].ToString()); //วันที่
            PutDataintoCell("G1", _date[1].ToString()); //วันที่
            PutDataintoCell("H1", _month[0].ToString()); //เดือน
            PutDataintoCell("I1", _month[1].ToString()); //เดือน
            PutDataintoCell("J1", _year[0].ToString()); //ปี
            PutDataintoCell("K1", _year[1].ToString()); //ปี
            PutDataintoCell("L1", _year[2].ToString()); //ปี
            PutDataintoCell("M1", _year[3].ToString()); //ปี
            PutDataintoCell("B6", issueDatePicker.SelectedDate.Value.ToString("dd/MM/yyyy")); //วันที่ออกเช็ค
            PutDataintoCell("B6", PayeeTextBox.Text); //จ่ายให้
            PutDataintoCell("D8", AmountTextBox.Text);//จำนวนเงิน
            //PutDataintoCell("XX", RemarkTextBox.Text); //หมายเหตุ

            excelWorkSheet.PrintOutEx();
            excelWorkBook.Close(0);
            excelApplication.Quit();
        }

        private void PutDataintoCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Name_Payee = "";
                SearchPayee window = new SearchPayee();
                window.ShowDialog();
                if (window.SelectedPayeeName == null) return;
                PayeeTextBox.Text = "";
                ID_Payee = window.SelectedPayeeName.Id;
                Name_Payee = window.SelectedPayeeName.Payee.ToString();
                //PayeeTextBox.Text = "[" + ID_Payee + "]" + Name_Payee;
                PayeeTextBox.Text = Name_Payee;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            issueDatePicker.Text = "";
            PayeeTextBox.Text = "";
            AmountTextBox.Text = "";
            RemarkTextBox.Text = "";
        }
    }
}
