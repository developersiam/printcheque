﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PrintChequeDAL;
using DomainModel;
using PrintChequeBL;

namespace PrintCheque.Forms
{
    public partial class Payee : Page
    {
        public Payee()
        {
            InitializeComponent();
            ShowAllPayee();
        }
        private void ShowAllPayee()
        {
            PayeeDataGrid.ItemsSource = null;
            var payee = BussinessLayerService.PayeeCheque().GetAllPayee();
            if (payee != null) PayeeDataGrid.ItemsSource = payee;
        }
        private void PayeeDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BlindSelectedData();
        }
        void BlindSelectedData()
        {
            try
            {
                var selected = (PS_CHEQUE_PAYEE)PayeeDataGrid.SelectedItem;
                if (selected != null)
                {
                    Id.Text = selected.Id.ToString();
                    PayeeName.Text = selected.Payee;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Id.Text))
            {
                //check existed
                var existed = BussinessLayerService.PayeeCheque().ExistedPayee(Convert.ToInt32(Id.Text));
                if (existed)
                {
                    var existedPayee = BussinessLayerService.PayeeCheque().GetPayee(Convert.ToInt32(Id.Text));
                    existedPayee.Payee = PayeeName.Text;
                    BussinessLayerService.PayeeCheque().UpdatePayee(existedPayee);
                }
            }
            else
            {
                //GetMax
                var maxID = BussinessLayerService.PayeeCheque().GetMaxPayeeID();
                var newPayee = new PS_CHEQUE_PAYEE
                {
                    Id = maxID,
                    Payee = PayeeName.Text
                };
                BussinessLayerService.PayeeCheque().AddNewPayee(newPayee);
            }
            ShowAllPayee();

        }
        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            Id.Text = "";
            PayeeName.Text = "";
            ShowAllPayee();
        }
    }
}
