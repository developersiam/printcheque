﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using PrintChequeBL;
using DomainModel;

namespace PrintCheque.Forms
{
    public partial class HistoryPrintCheque : Page
    {
        public HistoryPrintCheque()
        {
            InitializeComponent();
        }
        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            issueDatePicker.SelectedDate.GetValueOrDefault();
            //issueDatePicker.DisplayDate = DateTime.Today;
            SearchAllCheckBox.IsChecked = false;
            HistoryDataGrid.ItemsSource = null;
        }

        private void issueDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SearchAllCheckBox.IsChecked = false;
            ShowPrintChequeHistory();
        }
        private void ShowPrintChequeHistory()
        {
            try
            {
                //Show history filter by Date
                HistoryDataGrid.ItemsSource = null;
                List<Cheque_Details> HistoryCheque = new List<Cheque_Details>();
                HistoryCheque = BussinessLayerService.PayeeChequeDetails().GetPayeeChequeDetails((DateTime)issueDatePicker.SelectedDate);
               
                HistoryDataGrid.ItemsSource = HistoryCheque;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void ShowAllPrintChequeHistory()
        {
            try
            {
                //Show history filter by Date
                HistoryDataGrid.ItemsSource = null;
                List<Cheque_Details> HistoryCheque = new List<Cheque_Details>();
                HistoryCheque = BussinessLayerService.PayeeChequeDetails().GetPayeeChequeAll();

                HistoryDataGrid.ItemsSource = HistoryCheque;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchAllCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (SearchAllCheckBox.IsChecked == true)
            {
                issueDatePicker.SelectedDate.GetValueOrDefault();
                ShowAllPrintChequeHistory();
            };
        }

        private void SearchAllCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            HistoryDataGrid.ItemsSource = null;
        }
    }
}
