﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using System.Data.Entity;

namespace PrintChequeDAL
{
    public interface IPayeeChequeRepository : IGenericDataRepository<PS_CHEQUE_PAYEE> { }
    public class payeeChequeRepository : GenericDataRepository<PS_CHEQUE_PAYEE>, IPayeeChequeRepository { }
    public interface IPayeeChequeDetailsRepository : IGenericDataRepository<PS_CHEQUE_DETAILS> { }
    public class payeeChequeDetailsRepository : GenericDataRepository<PS_CHEQUE_DETAILS>, IPayeeChequeDetailsRepository { }

}
