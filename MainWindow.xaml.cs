﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PrintCheque
{
    public partial class MainWindow : Window
    {
        string user = "";
        public MainWindow(string User)
        {
            InitializeComponent();
            user = User;
        }

        private void PayeeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.Payee());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.PrintCheque(user));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void HistoryMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.HistoryPrintCheque());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitMenu_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ต้องการออกจากโปรแกรมใช่หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Close();
        }
    }
}
