﻿using System;
using System.Windows;
using System.Windows.Input;
using PrintCheque.shared;

namespace PrintCheque
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Login();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            try
            {
                if (!string.IsNullOrEmpty(UsernameTextbox.Text) || !string.IsNullOrEmpty(PasswordTextbox.Password))
                {
                    if (string.IsNullOrEmpty(UsernameTextbox.Text)) throw new Exception("กรุณาระบุชื่อผู้ใช้งาน");
                    if (string.IsNullOrEmpty(PasswordTextbox.Password)) throw new Exception("กรุณาระบุรหัสผ่าน");
                    if (PrintChequeService.UserAuthentication().GetLogin(UsernameTextbox.Text, PasswordTextbox.Password))
                    {
                        if (PrintChequeService.UserAuthentication().PermissionDepartmentCheck())
                        {
                            MainWindow main = new MainWindow(UsernameTextbox.Text);
                            main.Title = string.Format("Print Cheque - [{0}]", UsernameTextbox.Text);
                            main.ShowDialog();
                            Clear();
                        }
                        else
                            MessageBox.Show("คุณไม่ได้รับอนุญาตในการใช้โปรแกรมนี้", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                //else
                //{
                //    MainWindow main = new MainWindow();
                //    main.Title = string.Format("Print Cheque - [{0}]", "Username");
                //    main.ShowDialog();
                //    Clear();
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            UsernameTextbox.Text = "";
            PasswordTextbox.Password = "";
            UsernameTextbox.Focus();
        }
    }
}
