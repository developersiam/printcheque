﻿using System;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCheque.shared
{
    public interface IUserAuthentication
    {
        bool GetLogin(string username, string password);
        bool PermissionDepartmentCheck();
    }

    public class UserAuthentication : IUserAuthentication
    {
        public bool GetLogin(string username, string password)
        {
            try
            {
                SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
                PrincipalContext domain = new PrincipalContext(ContextType.Domain, null, username, password);
                UserPrincipal user = UserPrincipal.FindByIdentity(domain, username);
                DirectoryEntry directoryEntry = (user.GetUnderlyingObject() as DirectoryEntry);
                //Get Email
                _singleton.Email = user.EmailAddress;
                _singleton.Username = username;
                _singleton.LoggedIn = DateTime.Now;
                if (directoryEntry != null)
                {
                    string[] directoryEntryPath = directoryEntry.Path.Split(',');
                    foreach (var splitedPath in directoryEntryPath)
                    {
                        string[] elements = splitedPath.Split('=');
                        if (elements[0].Trim() == "OU")
                        {
                            //Get Department
                            _singleton.DepartmentCode = elements[1].Trim();
                            break;
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool PermissionDepartmentCheck()
        {
            SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
            if (_singleton.DepartmentCode == "ACC" || _singleton.DepartmentCode == "IT") return true;
            else return false;
        }


    }


}
