﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace PrintChequeBL
{
    public static class BussinessLayerService
    {
        public static IPayeeCheque PayeeCheque()
        {
            IPayeeCheque obj = new PayeeChequeBL();
            return obj;
        }
        public static IPayeeChequeDetails PayeeChequeDetails()
        {
            IPayeeChequeDetails obj = new PayeeChequeDetailsBL();
            return obj;
        }
    }
}
