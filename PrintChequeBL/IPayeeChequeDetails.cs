﻿using System.Collections.Generic;
using DomainModel;
using PrintChequeDAL;
using System.Linq;
using System;

namespace PrintChequeBL
{
    public interface IPayeeChequeDetails
    {
        List<Cheque_Details> GetPayeeChequeDetails(DateTime issueDate);

        List<Cheque_Details> GetPayeeChequeAll();
        void InsertChequeDetails(PS_CHEQUE_DETAILS items);
    }
    public class PayeeChequeDetailsBL : IPayeeChequeDetails
    {
        public List<Cheque_Details> GetPayeeChequeDetails(DateTime issueDate)
        {
            //return DataAccessLayerServices.PayeeChequeDetailsRepository().GetList(s => s.Paid_date == issueDate).OrderByDescending(a => a.Paid_date).ToList();
            List<Cheque_Details> results = new List<Cheque_Details>();
            var payee = DataAccessLayerServices.PayeeChequeRepository().GetAll();
            var details = DataAccessLayerServices.PayeeChequeDetailsRepository().GetAll();

            var result = details.OrderByDescending(a => a.Paid_date)
                        .Join(payee, d => d.Id, p => p.Id, (d, p) => new
                        {
                            d.Paid_date,
                            p.Payee,
                            d.Amount,
                            d.Remark,
                            d.Ddate,
                            d.Ttime
                        })
                        .Where(s => s.Paid_date == issueDate).ToList();


            foreach (var items in result)
            {
                results.Add(new Cheque_Details
                {
                    Paid_date = items.Paid_date,
                    Name = items.Payee.ToString(),
                    Amount = items.Amount,
                    Remark = items.Remark.ToString(),
                    Ddate = items.Ddate,
                    Ttime = items.Ttime

                });
            }
            return results;
        }

        public List<Cheque_Details> GetPayeeChequeAll()
        {
            List<Cheque_Details> results = new List<Cheque_Details>();
            var payee = DataAccessLayerServices.PayeeChequeRepository().GetAll();
            var details = DataAccessLayerServices.PayeeChequeDetailsRepository().GetAll();

            var result = details.OrderByDescending(q => q.Paid_date)
                        .Join(payee, d => d.Id, p => p.Id, (d, p) => new
                        {
                            d.Paid_date,
                            p.Payee,
                            d.Amount,
                            d.Remark,
                            d.Ddate,
                            d.Ttime
                        }).ToList();

            foreach (var items in result)
            {
                results.Add(new Cheque_Details
                {
                    Paid_date = items.Paid_date,
                    Name = items.Payee.ToString(),
                    Amount = items.Amount,
                    Remark = items.Remark.ToString(),
                    Ddate = items.Ddate,
                    Ttime = items.Ttime
                });
            }
            return results;
        }
        public void InsertChequeDetails(PS_CHEQUE_DETAILS items)
        {
            DataAccessLayerServices.PayeeChequeDetailsRepository().Add(items);
        }


    }
}