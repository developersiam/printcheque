﻿using System;
using System.Collections.Generic;
using DomainModel;
using PrintChequeDAL;
using System.Linq;

namespace PrintChequeBL
{
    public interface IPayeeCheque
    {
        List<PS_CHEQUE_PAYEE> GetAllPayee();
        bool ExistedPayee(int id);
        int GetMaxPayeeID();
        PS_CHEQUE_PAYEE GetPayee(int id);
        void AddNewPayee(PS_CHEQUE_PAYEE payee);
        void UpdatePayee(PS_CHEQUE_PAYEE payee);
        List<PS_CHEQUE_PAYEE> GetListPayeeByFilter(string Pname);
    }
    public class PayeeChequeBL : IPayeeCheque
    {
        public List<PS_CHEQUE_PAYEE> GetAllPayee()
        {
            return DataAccessLayerServices.PayeeChequeRepository().GetAll().ToList() ;
        }
        public bool ExistedPayee(int id)
        {
            bool existed = false;
            payeeChequeRepository p = new payeeChequeRepository();
            var payee = p.GetList(w => w.Id == id);
            if (payee.Count() > 0)
            {
                existed = true;
            }
            return existed;
        }
        public int GetMaxPayeeID()
        {
            int maxID = 0;
            payeeChequeRepository p = new payeeChequeRepository();
            maxID = p.GetList(s => s.Id > 0).Max(t => t.Id) + 1;
            return maxID;
        }
        public void AddNewPayee(PS_CHEQUE_PAYEE items)
        {
            DataAccessLayerServices.PayeeChequeRepository().Add(items);
        }
        public void UpdatePayee(PS_CHEQUE_PAYEE items)
        {
            DataAccessLayerServices.PayeeChequeRepository().Update(items);
        }
        public PS_CHEQUE_PAYEE GetPayee(int id)
        {
            return DataAccessLayerServices.PayeeChequeRepository().GetSingle(w => w.Id == id);
        }
        public List<PS_CHEQUE_PAYEE> GetListPayeeByFilter(string PName)
        {
            return DataAccessLayerServices.PayeeChequeRepository().GetList(g => g.Payee.Contains(PName)).ToList();
        }
    }
}
